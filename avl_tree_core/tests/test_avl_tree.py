import avl_tree.core as avl
from avl_tree.core import AVLTreeNode, Sign
from collections import defaultdict, deque
from itertools import pairwise
import pytest
import random
import string


def is_sorted(iterable):
    return all(a <= b for (a, b) in pairwise(iterable))


def check_parent_consistency(nodes):
    for node in nodes:
        for child in avl.children(node):
            assert child.parent is node
        if node.parent is not None:
            assert (node is node.parent.left
                    or node is node.parent.right)


def check_subtree_props(root):
    for node in reversed(list(bfs(root))):
        old_subtree_props = node.subtree_props
        avl.update_subtree_props(node)
        assert node.subtree_props == old_subtree_props


def random_leaf(root):
    node = root
    while not avl.is_leaf(node):
        node = random.choice(avl.children(node))
    return node


def bfs(root: AVLTreeNode):
    q = deque([root])
    while q:
        node = q.popleft()
        q.extend(avl.children(node))
        yield node


def test_parent_consistency_and_order_stability():
    random.seed(1234)

    # Build the tree
    nodes = [AVLTreeNode(key=0, display_name=c)
             for c in string.ascii_lowercase[:20]]
    for next_node in nodes[1:]:
        random_leaf(nodes[0].root())._insert_leaf(
                next_node, random.choice(list(avl.Sign)))

    check_parent_consistency(nodes)
    reverse_index = {node.display_name: i
                     for (i, node) in enumerate(nodes[0].root().dfs())}

    # Delete half the nodes in the tree
    random.shuffle(nodes)
    second_half = slice(None, len(nodes) // 2)
    for node in nodes[second_half]:
        node.remove()
    del nodes[second_half]

    check_parent_consistency(nodes)

    # Check that order of the remaining nodes is preserved
    ords = [reverse_index[node.display_name]
            for node in nodes[0].root().dfs()]
    assert len(ords) < len(reverse_index)
    assert is_sorted(ords)


def test_key_order():
    random.seed(1234)
    keys = list(string.ascii_lowercase[:20])
    random.shuffle(keys)
    nodes = [AVLTreeNode(key=c, display_name=c)
             for c in keys]
    assert not is_sorted([node.key for node in nodes])
    for node in nodes[1:]:
        nodes[0].root().insert(node)
    assert is_sorted([node.key for node in nodes[0].root().dfs()])

    random.shuffle(nodes)
    second_half = slice(None, len(nodes) // 2)
    for node in nodes[second_half]:
        node.remove()
    del nodes[second_half]
    assert is_sorted([node.key for node in nodes[0].root().dfs()])


@pytest.mark.parametrize("tiebreak", [Sign.POS, Sign.NEG])
def test_key_order_with_duplicates(tiebreak):
    random.seed(2345)
    keys = list("aaaabbbbcde")
    random.shuffle(keys)
    nodes = [AVLTreeNode(key=c, display_name=c)
             for c in keys]
    for (i, node) in enumerate(nodes):
        node.data = {"seq_num": i}
    assert not is_sorted([node.key for node in nodes])
    for node in nodes[1:]:
        nodes[0].root().insert(node, tiebreak=tiebreak)

    # Assert that DFS traverses the keys in order
    root = nodes[0].root()
    assert is_sorted([node.key for node in root.dfs()])

    # Assert that for each key, the relative order in which nodes having that
    # key appear in a DFS is the same as insertion order (if `tiebreak == POS`)
    # or reverse insertion order (if `tiebreak == NEG`)
    per_key_seqs = defaultdict(list)
    for node in root.dfs():
        per_key_seqs[node.key].append(node.data["seq_num"])
    assert len(per_key_seqs) == len(set(keys))
    assert sum(len(seq) for seq in per_key_seqs.values()) == len(keys)
    for seq_nums in per_key_seqs.values():
        assert is_sorted({Sign.POS: list,
                          Sign.NEG: reversed}[tiebreak](seq_nums))


def test_prev_next():
    keys = list("aaabcdeefg")
    nodes = [AVLTreeNode(key=c, display_name=c)
             for c in keys]
    for next_node in nodes[1:]:
        nodes[0].root().insert(next_node)

    nodes_fwd = []
    n = nodes[0].root().min_descendant()
    while n is not None:
        nodes_fwd.append(n)
        n = n.next()
    assert all(node is node_
               for (node, node_) in zip(nodes, nodes_fwd))

    nodes_bwd = []
    n = nodes[0].root().max_descendant()
    while n is not None:
        nodes_bwd.append(n)
        n = n.prev()
    assert all(node is node_
               for (node, node_) in zip(nodes, reversed(nodes_bwd)))


def test_nth():
    keys = list("aaabcdeefg")
    nodes = [AVLTreeNode(key=c, display_name=c)
             for c in keys]
    for next_node in nodes[1:]:
        nodes[0].root().insert(next_node)
    nth_keys = [nodes[0].root().get_at(i).key for i in range(len(keys))]
    assert nth_keys == keys


def test_balance():
    random.seed(1234)
    d = 10
    n = 2**d
    height_bound = 2*d
    # Sanity check, can't be shorter than a full binary tree
    height_lower_bound = d + 1
    nodes = [AVLTreeNode(key=random.randint(0, 1_000_000))
             for _ in range(n)]
    for node in nodes[1:]:
        nodes[0].root().insert(node)
    height = nodes[0].root().subtree_props["height"]
    assert height_lower_bound <= height and height <= height_bound
    check_subtree_props(nodes[0].root())

    new_n = 2**(d-1)
    new_height_bound = 2*(d-1)
    new_height_lower_bound = d
    random.shuffle(nodes)
    for node in nodes[new_n:]:
        node.remove()
    del nodes[new_n:]
    height = nodes[0].root().subtree_props["height"]
    assert new_height_lower_bound <= height and height <= new_height_bound
    check_subtree_props(nodes[0].root())

    replacement_nodes = [AVLTreeNode(key=random.randint(0, 1_000_000))
                         for _ in range(n - new_n)]
    for node in replacement_nodes:
        nodes[0].root().insert(node)
    nodes.extend(replacement_nodes)
    height = nodes[0].root().subtree_props["height"]
    assert height_lower_bound <= height and height <= height_bound
    check_subtree_props(nodes[0].root())
