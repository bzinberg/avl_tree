"""AVL tree implementation."""

from __future__ import annotations

from collections import deque
from enum import Enum
from itertools import accumulate, pairwise, repeat, takewhile
from typing import Optional


def remove_head(it):
    """Advances `it` by one element and returns `it`, for chainability."""
    next(it)
    return it


def last(it):
    """Returns the last element of `it`.

    This is a specialization of the `tail` recipe from itertools:
    https://docs.python.org/3/library/itertools.html#itertools-recipes
    """
    return deque(it, maxlen=1).pop()


def first_true(iterable, /, predicate, *, default):
    """Returns the first value `x` in `iterable` such that `predicate(x)` is
    true, or `default` if there is no such value.

    This function is adapted from the `first_true` recipe from itertools:
    https://docs.python.org/3/library/itertools.html#itertools-recipes
    """
    return next(filter(predicate, iterable), default)


def is_leaf(node: AVLTreeNode):
    return (node.left is None
            and node.right is None)


def children(node):
    "Returns the nonempty child subtrees of `node`."
    return [c for c in (node.left, node.right)
            if c is not None]


def ancestors_asc(node: AVLTreeNode):
    """Returns an iterator over the ancestors of `node`, in ascending order
    from `node` (inclusive) to the root of the tree."""
    return takewhile(lambda x: x is not None,
                     accumulate(repeat("unused value"),
                                lambda x, _: x.parent,
                                initial=node))


# Subtree Properties
# ==================
# A "subtree property" is a (pure) function P of subtrees such that for any
# subtree `t`, the value of P(t) depends only on P(t's children), i.e., on
# P(t.left) and P(t.right).
#
# We keep a global registry of subtree properties, initially empty, with new
# entries added via the decorator `@subtree_prop(name)`.
_SUBTREE_PROPS = {}


def subtree_prop(prop_name):
    """Returns a decorator that registers a subtree property with name
    `prop_name` that is computed via the rule

        val = f(left_val, right_val)

    where:

    - `val` is the value of the property for the subtree rooted at node `x`,
      and is not `None`.
    - `left_val` is the value of the property for the subtree rooted at
      `x.left`, and is `None` if and only if `x.left` is `None`.
    - Similarly for `right_val`.
    """
    def decorate(f):
        _SUBTREE_PROPS[prop_name] = f
        return f
    return decorate


def get_subtree_prop(node: Optional[AVLTreeNode], k: str):
    if node is None:
        return None
    return node.subtree_props[k]


def compute_subtree_prop(k: str, left: Optional[AVLTreeNode],
                         right: Optional[AVLTreeNode]):
    return _SUBTREE_PROPS[k](get_subtree_prop(left, k),
                             get_subtree_prop(right, k))


def update_subtree_prop(node: AVLTreeNode, k: str):
    node.subtree_props[k] = _SUBTREE_PROPS[k](get_subtree_prop(node.left, k),
                                              get_subtree_prop(node.right, k))


def update_subtree_props(node: AVLTreeNode):
    for k in _SUBTREE_PROPS:
        update_subtree_prop(node, k)


# Height
# ------
# The height of a leaf is zero.  The height of a non-leaf subtree is 1 plus the
# max height of its children.
@subtree_prop("height")
def _compute_height(left_height, right_height):
    if left_height is None and right_height is None:
        return 0
    return 1 + max(left_height or 0, right_height or 0)


def skew(node: AVLTreeNode) -> int:
    # `ℓ` (resp. `r`) is the height of the tree that would result if we deleted
    # the right (resp. left) subtree of `node`.
    ℓ = (0 if node.left is None
         else 1 + get_subtree_prop(node.left, "height"))
    r = (0 if node.right is None
         else 1 + get_subtree_prop(node.right, "height"))
    return r - ℓ


def is_height_balanced(node: AVLTreeNode) -> bool:
    s = skew(node)
    return (-1 <= s and s <= 1)


def is_height_unbalanced(node: AVLTreeNode) -> bool:
    return not is_height_balanced(node)


# Size
# ----
# The size of a subtree is the number of nodes in the subtree.
@subtree_prop("size")
def _compute_size(left_size, right_size):
    return 1 + (left_size or 0) + (right_size or 0)


# === Signs ===

class Sign(Enum):
    """Sign (positive or negative).

    Useful for unifying code that has chirality: insetad of explicitly
    implementing the mirror image of a function, we can often instead add a
    sign parameter and genericize the function to operate in mirror image when
    `sign == NEG`, without duplicating the existing code.
    """
    POS = 1
    NEG = 2

    def __neg__(self):
        if self == Sign.POS:
            return Sign.NEG
        else:
            assert self == Sign.NEG
            return Sign.POS


def get_child(node: AVLTreeNode, sign: Sign) -> Optional[AVLTreeNode]:
    """Get the right (if `sign == POS`) or left (if `sign == NEG`) child of
    `node`."""
    if sign == Sign.POS:
        return node.right
    else:
        assert sign == Sign.NEG
        return node.left


def _which_child(node: AVLTreeNode, default: Optional[Sign] = None) -> Sign:
    """Returns `NEG` if `node` is the left child of its parent, or `POS` if
    `node` is the right child of its parent.  Thus, whenever `node.parent` is
    not `None`, we have the round-trip invariant

        get_child(node.parent, _which_child(node)) is node

    If `default` is supplied, then `default` will be returned in the case where
    `node.parent` is `None`.  If `default` is not supplied, then `node.parent`
    being `None` results in an exception.
    """
    if node.parent is None:
        assert default is not None
        return default
    if node is node.parent.left:
        return Sign.NEG
    else:
        assert node is node.parent.right
        return Sign.POS


# === Mutating Trees ===


def _assign_child_raw(node: AVLTreeNode,
                      child: Optional[AVLTreeNode],
                      which: Sign):
    """Assign `child` as the right (if `which == POS`) or left (if `which ==
    NEG`) child of `node`.

    NOTE: This function does *not* reassign parent pointer of `child`, and does
    *not* modify any fields of the old occupant of the replaced child slot.
    After calling this function, the caller must also update those pointers to
    ensure that parent/child pointers are consistent and there are no cycles.
    """
    if which == Sign.NEG:
        node.left = child
    else:
        assert which == Sign.POS
        node.right = child


def _replace_child_subtree(node: Optional[AVLTreeNode],
                           new_child: Optional[AVLTreeNode],
                           which: Sign):
    """(Re)-assign the right (if `which == POS`) or left (if `which == NEG`)
    subtree of `node` to be the subtree rooted at `new_child`, and adjust
    parent and child pointers accordingly.

    Returns the subtree that formerly occupied the spot being assigned (or
    `None` if that subtree was empty).  If that subtree was nonempty, then its
    root (the old child node) will have its parent pointer changed to `None`.

    The full set of attributes modified by this function (if the part before
    the dot is not `None`) is:

        - node.{left if which==POS, right if which==NEG}
        - new_child.parent
        - old_child.parent.{left or right, whichever one is old_child}
        - old_child.parent

    To avoid ambiguous behavior, we disallow replacing a child with itself
    (`new_child` cannot itself be the old child).

    NOTE: This function does not maintain height balance or subtree properties;
    the caller must do that themself.
    """
    old_child = (get_child(node, which)
                 if node is not None else None)
    assert old_child is not new_child, (
            "Attempted to replace a child node with itself.  To avoid"
            " ambiguity, please do not do this.")

    if node is not None:
        _assign_child_raw(node, new_child, which)
    if new_child is not None:
        if new_child.parent is not None:
            _assign_child_raw(new_child.parent, None, _which_child(new_child))
        new_child.parent = node

    if old_child is not None:
        old_child.parent = None
    return old_child


def _replace_left_subtree(node: AVLTreeNode, left: Optional[AVLTreeNode]):
    _replace_child_subtree(node, left, Sign.NEG)


def _replace_right_subtree(node: AVLTreeNode, right: Optional[AVLTreeNode]):
    _replace_child_subtree(node, right, Sign.POS)


# === Tree Objects ===


class AVLTreeNode:
    """Class representing a node in an AVL tree.

    Each node has a `key`.  All keys within a tree must be comparable to each
    other (for example, they could be ints, strings or tuples), and the tree is
    a self-balancing binary search tree whose order is determined by the keys.

    The children of a node `x` are `x.left` and `x.right`, either or both of
    which may be `None` to indicate that a child slot is vacant.  The parent of
    `x` is `x.parent`, which is `None` if and only if `x` is the root of the
    tree.  Note that as the tree is self-balancing, the root may change after
    an insertion or deletion operation.  See `.root()` for finding the root.

    In applications, one often wants to attach satellite data to the nodes of a
    tree; this can be done by supplying the `data` keyword argument to the
    constructor (or just setting the `data` attribute directly).
    """
    def __init__(self,
                 *,
                 key,
                 data=None,
                 display_name: Optional[str] = None):
        self.key = key
        self.data = data
        self.parent = None
        self.left = None
        self.right = None
        self.display_name = (display_name or str(id(self)))
        self.subtree_props = {k: compute_subtree_prop(k, None, None)
                              for k in _SUBTREE_PROPS}

    def __repr__(self):
        return f"<AVLTreeNode {repr(self.display_name)}>"

    def root(self) -> AVLTreeNode:
        """Return the root of the tree containing this node, by walking
        `.parent`s until a node with no parent is found."""
        return last(ancestors_asc(self))

    def _rotate(self, sign: Sign):
        """Rotate the (sub)tree rooted at this node to the right (if `sign ==
        POS`) or left (if `sign == NEG`).

        Updates subtree properties for the three nodes whose subtrees are
        modified in this process.  Thus, if the subtree properties of all
        descendants of `self` are up-to-date before this method call, then they
        will also be up-to-date after this method call.

        If `self` is a subtree of a larger tree, then its (strict) ancestors
        may need to have their subtree properties updated after this method
        call.
        """
        assert get_child(self, -sign) is not None, (
                "Invalid rotation: the child that would get promoted to the"
                " top of the tree is missing")
        p = self.parent
        if p is None:
            def p_assign_child(_, child):
                child.parent = None
        elif p.left is self:
            p_assign_child = _replace_left_subtree
        else:
            assert p.right is self
            p_assign_child = _replace_right_subtree
        b = get_child(self, -sign)
        c = get_child(b, sign)
        _replace_child_subtree(self, c, -sign)
        _replace_child_subtree(b, self, sign)
        p_assign_child(p, b)
        # Update subtree props from childmost to parentmost.
        # c is child of self, self is child of b.
        if c is not None:
            update_subtree_props(c)
        update_subtree_props(self)
        update_subtree_props(b)

    def _local_rebalance(self):
        """Rebalance the subtree rooted at this node, assuming the following
        requirements:

        - The skew is either 2 or -2 (the tree is just barely unbalanced).
        - Every subtree rooted at a (strict) descendant of this node is
          height-balanced.

        This method assigns `self` a new parent P.  After this method call, the
        descendants of P are precisely all nodes that used to be descendants of
        `self`.  Additionally, the subtree properties of all these nodes are up
        to date.

        Thus, if `self` is a subtree of a larger tree, then after this method
        call, the nodes whose subtree properties may need updating are
        precisely the (strict) ancestors of P.
        """
        skew_sign = { 2: Sign.POS,
                     -2: Sign.NEG}[skew(self)]
        f = get_child(self, skew_sign)
        assert f is not None
        the_complicated_fskew = {Sign.POS: -1,
                                 Sign.NEG: 1}[skew_sign]
        if skew(f) == the_complicated_fskew:
            f._rotate(skew_sign)
        self._rotate(-skew_sign)

    def _insert_leaf(self, new_node: AVLTreeNode, sign: Sign):
        """Insert leaf `new_node` into the tree as the right (if `sign == POS`)
        or left (if `sign == NEG`) child of this node, and rebalance the tree
        (including ancestors of this node) if necessary.

        The spot at which the `new_node` is to be inserted must be vacant.
        """
        assert is_leaf(new_node), "Input must be a leaf"
        assert get_child(self, sign) is None, (
                "Tried to insert a subtree in a spot where there was already"
                " an existing subtree")
        _replace_child_subtree(self, new_node, sign)
        for node in ancestors_asc(new_node):
            update_subtree_props(node)
        lowest_unbalanced_ancestor = next(filter(is_height_unbalanced,
                                                 ancestors_asc(new_node)),
                                          None)
        if lowest_unbalanced_ancestor is not None:
            lowest_unbalanced_ancestor._local_rebalance()
            # Implementation note: Could keep track of which individual
            # properties have changed, whittling down a set of keys that starts
            # as keys(_SUBTREE_PROPS) and stopping the loop when that set
            # becomes empty.  That's probably over-optimization, though.
            for node in remove_head(ancestors_asc(lowest_unbalanced_ancestor)):
                update_subtree_props(node)

    def _insert_leaf_left(self, new_left: AVLTreeNode):
        self._insert_leaf(new_left, Sign.NEG)

    def _insert_leaf_right(self, new_right: AVLTreeNode):
        self._insert_leaf(new_right, Sign.POS)

    def remove(self):
        """Remove this node from the tree, and rebalance the tree if necessary.

        Note that while the traversal order of the other nodes in the tree is
        unchanged by this removal, the shape of the tree may change.
        """
        p = self.parent
        self_position = _which_child(self, default=Sign.POS)
        cs = children(self)
        if not cs:
            assert p is not None, (
                "It's not clear what mutating-remove means when the tree has"
                " only one node - I can't mutate a `None`")
            _replace_child_subtree(p, None, self_position)
            lowest_modified_subtree = p
        elif len(cs) == 1:
            c, = cs
            _replace_child_subtree(p, c, self_position)
            lowest_modified_subtree = p
        else:
            succ = self.next()
            assert succ.left is None
            if succ is self.right:
                ℓ = self.left
                _replace_left_subtree(succ, ℓ)
                _replace_child_subtree(p, succ, self_position)
                lowest_modified_subtree = succ
            else:
                assert succ is succ.parent.left
                # Let's grab this pointer while it still has a name.  After
                # this else-block, it will be the lowest modified subtree.
                lowest_modified_subtree = succ.parent
                r = succ.right
                _replace_left_subtree(succ.parent, r)
                assert is_leaf(succ)
                _replace_left_subtree(succ, self.left)
                _replace_right_subtree(succ, self.right)
                _replace_child_subtree(self.parent, succ, self_position)
        assert is_leaf(self)

        if lowest_modified_subtree is not None:
            for node in ancestors_asc(lowest_modified_subtree):
                update_subtree_props(node)
                if is_height_unbalanced(node):
                    node._local_rebalance()

        return self

    def minmax_descendant(self, sign: Sign):
        """Return the minimum (if `sign == NEG`) or maximum (if `sign == POS`)
        node in the (sub)tree rooted at this node."""
        return last(takewhile(lambda x: x is not None,
                              accumulate(repeat("unused value"),
                                         lambda x, _: get_child(x, sign),
                                         initial=self)))

    def min_descendant(self):
        "Return the minimum node in the (sub)tree rooted at this node."
        return self.minmax_descendant(Sign.NEG)

    def max_descendant(self):
        "Return the maximum node in the (sub)tree rooted at this node."
        return self.minmax_descendant(Sign.POS)

    def prev_or_next(self, sign: Sign) -> AVLTreeNode:
        """Return the node immediately before (if `sign == NEG`) or after (if
        `sign == POS`) this node in traversal order.  If no such node exists
        (i.e., if this node is the first or last node in the tree
        respectively), return `None`.
        """
        if get_child(self, sign) is not None:
            return get_child(self, sign).minmax_descendant(-sign)

        def is_sought_child_position(t):
            (child, parent) = t
            return get_child(parent, -sign) is child

        (_, parent) = first_true(pairwise(ancestors_asc(self)),
                                 predicate=is_sought_child_position,
                                 default=(None, None))
        return parent

    def prev(self) -> AVLTreeNode:
        """Return the node immediately before this node in traversal order, or
        `None` if this is the first node in the tree."""
        return self.prev_or_next(Sign.NEG)

    def next(self) -> AVLTreeNode:
        """Return the node immediately after this node in traversal order, or
        `None` if this is the last node in the tree."""
        return self.prev_or_next(Sign.POS)

    def dfs(self):
        """"Yields all nodes in the (sub)tree rooted at this node, in
        depth-first, "in-order" traversal order."""
        s = deque([(self, False)])
        while s:
            (node, already_pushed_children) = s.pop()
            if not already_pushed_children and node.right is not None:
                s.append((node.right, False))
            if already_pushed_children:
                yield node
            else:
                s.append((node, True))
            if not already_pushed_children and node.left is not None:
                s.append((node.left, False))

    def find_or_neighbor(self, k, tiebreak: Sign = Sign.POS, *,
                         _key_xform=None):
        """If key `k` occurs in the (sub)tree rooted at this node, returns the
        node with key `k`.  If there are multiple such nodes, the latest (if
        `tiebreak == POS`, the default) or earliest (if `tiebreak == NEG`) such
        node in traversal order.

        Otherwise, returns the unique node `n` such that inserting a leaf node
        with key `k` as a child of `n` (the left child if `k < n.key`, the
        right child right if `k > n.key`) is a valid operation.
        """
        node = self
        while True:
            cur_k = (node.key if _key_xform is None
                     else _key_xform(node.key))
            if cur_k == k:
                break
            if ((cur_k > k and node.left is None)
                    or (cur_k < k and node.right is None)):
                return node
            if cur_k > k:
                node = node.left
            elif cur_k < k:
                node = node.right
            else:
                raise Exception("Bug: unhandled case")

        # `node` is now the lowest-depth node that has key `k`.  From here we
        # proceed to find the latest or earliest (according to `sign`) node
        # that has key `k`.  One can prove that the node we're looking for is a
        # descendant of `node`.
        assert node.key == k
        assert (node.parent is None or node.parent.key != k)
        return node._extreme_descendant_with_same_key(tiebreak)

    def find_first(self, k):
        """Returns the first (in traversal order) node with key `k`, or `None`
        if there is no such node."""
        node = self.find_or_neighbor(k, tiebreak=Sign.NEG)
        if node.key == k:
            return node
        return None

    def find_last(self, k):
        """Returns the last (in traversal order) node with key `k`, or `None`
        if there is no such node."""
        node = self.find_or_neighbor(k, tiebreak=Sign.POS)
        if node.key == k:
            return node
        return None

    def _extreme_descendant_with_same_key(self, sign: Sign):
        """Returns the earliest (if `sign == NEG`) or latest (if `sign == POS`)
        descendant of this node that has the same key."""
        k = self.key
        best = self
        candidate = self
        while True:
            if candidate is None:
                break
            if candidate.key == k:
                best = candidate
                candidate = get_child(candidate, sign)
            else:
                candidate = get_child(candidate, -sign)
        return best

    def insert(self, node: AVLTreeNode, tiebreak: Sign = Sign.POS):
        """Insert `node` into the tree rooted at this node, with its position
        determined by `node.key`.  If there are other nodes in the tree with
        the same key, `node` will be inserted after (if `tiebreak == POS`) or
        before (if `tiebreak == NEG`) all such nodes.

        Requirements:
        * `node` must be a leaf.
        * This method may only be called on the root node of the tree, as the
          computed position of insertion may differ when looking only at a
          subtree as opposed to the whole tree.
        """
        assert is_leaf(node)
        assert self.parent is None, (
                "`insert` can only be called on the root of the tree")
        tiebreak_num = {Sign.POS: 1,
                        Sign.NEG: -1}[tiebreak]
        recipient = self.find_or_neighbor((node.key, tiebreak_num),
                                          _key_xform=lambda k: (k, 0))
        if node.key == recipient.key:
            recipient._insert_leaf(node, tiebreak)
        elif node.key > recipient.key:
            recipient._insert_leaf_right(node)
        else:
            assert node.key < recipient.key
            recipient._insert_leaf_left(node)

    def get_at(self, i: int) -> AVLTreeNode:
        """Returns the `i`th node (in traversal order) of the subtree rooted at
        this node.
        """
        assert 0 <= i and i < get_subtree_prop(self, "size")
        left_size = get_subtree_prop(self.left, "size") or 0
        if i == left_size:
            return self
        elif i < left_size:
            return self.left.get_at(i)
        else:
            return self.right.get_at(i - left_size - 1)
