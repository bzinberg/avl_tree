import collections
from operator import concat

from .avl_tree import AVLTreeNode

StackFrame = collections.namedtuple(
        "StackFrame", [
            # Either an AVLTreeNode, or a sentinel value corresponding to one
            # of the special kinds of frame (see `make_newline_frame` and
            # `_EMPTY_SUBTREE_FRAME`).
            "node",
            # Indentation level (number of preceding spaces) at which this node
            # should be printed.
            "indent"
            ])


def make_newline_frame(indent):
    """Create a sentinel stack frame representing "now make a new line and
    indent it with `indent` spaces"."""
    return StackFrame("_NEWLINE", indent)


def is_newline_frame(f):
    return f.node == "_NEWLINE"


# Sentinel stack frame representing an empty subtree.  We need this sentinel
# because such a subtree does not get printed the same way as a nonempty
# subtree.
_EMPTY_SUBTREE_FRAME = StackFrame(None, None)


def _make_frame(node, indent):
    if node is None:
        return _EMPTY_SUBTREE_FRAME
    return StackFrame(node=node, indent=indent)


def pprint(root: AVLTreeNode):
    """Pretty-prints the AVL tree rooted at `root`.

    The aesthetic of this pretty printer is "S-expressions without the closing
    parentheses."

    Advantages: easy to print to console, easy to read, well-suited to ordered
    trees (as opposed to something like GraphViz which has a powerful general
    graph layout engine that is hard to constrain to respect order).

    Disadvantages: harder to read if node names are long, hard to augment nodes
    to show auxiliarity data / node properties.

    Example output:

        (h (d (b
                 (c
              (f (e
        
           (l (j (i
        
              (o (n
                 (p

    In the above example, the root of the tree is `h`; the children of `h` are
    `l` (left) and `d` (right); node `j` has only a right child (`i`) and no
    left child.
    """
    stack = [StackFrame(node=root, indent=0)]
    while stack:
        f = stack.pop()
        if is_newline_frame(f):
            print(concat("\n", f.indent * " "), end="")
            continue
        if f is _EMPTY_SUBTREE_FRAME:
            continue
        head_text = f"({f.node.display_name} "
        print(head_text, end="")
        child_indent = f.indent + len(head_text)
        if (f.node.left, f.node.right) != (None, None):
            stack.extend([_make_frame(f.node.left, child_indent),
                          make_newline_frame(child_indent),
                          _make_frame(f.node.right, child_indent)])
    print("")
