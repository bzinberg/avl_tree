# AVL Tree

An [AVL tree](https://en.wikipedia.org/wiki/AVL_tree) implementation.  This
data structure is a self-balancing binary search tree that can be used
efficiently as an ordered multi-set.

Most of the notations and conventions used in this library are cribbed from MIT
OCW
[lecture notes](https://ocw.mit.edu/courses/6-006-introduction-to-algorithms-spring-2020/pages/lecture-notes/)
about this topic.

## Quickstart

Create some nodes:

```python
import avl_tree.core as avl
from avl_tree.core import AVLTreeNode

nodes = [AVLTreeNode(key=k, display_name=k)
         for k in "cegabdf"]
```

Combine them into a tree using `insert`:

```python
for next_node in nodes[1:]:
    # Re-find root each time, as tree shape may change on each insert
    nodes[0].root().insert(next_node)
```

Visualize the tree using the companion package `avl_tree.viz`:

```python
import avl_tree.viz as viz
root = nodes[0].root()
g = viz.to_graphviz(root)
g.render("example1", view=True, cleanup=True)
```

![Rendered graph](images/example1.png)

Or pretty-print the graph to the console:

```python
avl.pprint(root)

# (e (f (g 
#       
#    (c (d 
#       (b 
#          (a
```

Search the tree for a specific key:

```python
e = root.find_first("e")  # O(log(n))
e
# <AVLTreeNode 'e'>
```

From there, advance to the next key:

```python
e.next()  # O(log(n)), amortized O(1) when called repeatedly
# <AVLTreeNode 'f'>
```

Search for a key that doesn't exist:

```python
root.find_first("z")  # O(log(n))
# None
```

But find one of the closest existent nodes (in terms of order-rank) to that
key:
```python
root.find_or_neighbor("z")  # O(log(n))
# <AVLTreeNode 'g'>
```

Find the third element (zero-indexed) in the tree:

```python
root.get_at(3)  # O(log(n))
# <AVLTreeNode 'd'>
```

When there are duplicate keys, the relative ordering of nodes with the same key
is insertion order:
```python
import random

nodes = [AVLTreeNode(key=k, display_name=k)
         for k in "abcdeeeeefg"]
random.shuffle(nodes)
# Distinguish same-key nodes by assigning each node a sequence number
for (i, node) in enumerate(nodes):
    node.data = {"seq_num": i}

for next_node in nodes[1:]:
    nodes[0].root().insert(next_node)

root = nodes[0].root()
g = viz.to_graphviz(root,
                    # Show sequence numbers in the rendered graph
                    data_maxlen=20)
g.render("example2", view=True, cleanup=True)
```

![Rendered graph](images/example2.png)

Indeed, the sequence numbers for each key are sorted even though the sequence
numbers across different keys are not:

```python
e_sequence = []
node = root.find_first("e")
while node.key == "e":
    e_sequence.append(node.data["seq_num"])
    node = node.next()
node
# <AVLTreeNode 'f'>

e_sequence
# [0, 4, 7, 8, 10]

[(x.key, x.data["seq_num"]) for x in root.dfs()]
# [('a', 9), ('b', 6), ('c', 5), ('d', 3), ('e', 0), ('e', 4), ('e', 7),
#  ('e', 8), ('e', 10), ('f', 2), ('g', 1)]
```

This remains true even after removing some of those nodes:

```python
root.find_first("e").next().remove()
root = nodes[0].root()
[x.data["seq_num"] for x in root.dfs() if x.key == "e"]
# [0, 7, 8, 10]
```


## How to install

You may want to create a fresh virtualenv first:
```shell
python3 -m venv my_venv
source my_venv/bin/activate
```
Make sure you have relatively recent versions of `pip` and `build`:
```shell
python3 -m pip install --upgrade pip build
```
Then install the package: from the `avl_tree_core` package root directory, run
```shell
python3 -m pip install .
```

## Dev instructions

To run the tests, first install the test-only dependencies: from the
`avl_tree_core` package root directory,
```shell
python3 -m pip install '.[test]'
```
Then run the tests:
```shell
python3 -m pytest
```
