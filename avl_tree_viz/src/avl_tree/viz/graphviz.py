"""GraphViz visualization for AVL trees."""
from avl_tree.core import AVLTreeNode
import graphviz
from operator import concat
from typing import Optional


def children(node: AVLTreeNode):
    return [c for c in (node.left, node.right)
            if c is not None]


def viz_node_id(node: AVLTreeNode):
    "Returns a unique identifier for `node`."
    return str(id(node))


def truncate(s: str, maxlen: int) -> str:
    """Truncates `s` to at length at most `maxlen`.  If the result is not all
    of `s`, then an additional character is removed from the end and an
    ellipsis is appended, resulting in a string of length `maxlen`."""
    assert maxlen > 0
    if len(s) <= maxlen:
        return s
    else:
        return concat(s[:maxlen - 1], "…")


def viz_node_label(node: AVLTreeNode,
                   data_maxlen: Optional[int] = None,
                   show_subtree_props: bool = False) -> str:
    """Returns the label that should be used for this node in the GraphViz
    rendering.

    Arguments `data_maxlen` and `show_subtree_props` have the same meaning as
    in `to_graphviz`.
    """
    return "\n".join([node.display_name,
                      *([f"""data = {truncate(str(node.data),
                                              maxlen=data_maxlen)}"""]
                        if data_maxlen is not None else []),
                      *([f"{key}: {val}"
                         for (key, val) in node.subtree_props.items()]
                        if show_subtree_props else [])])


def to_graphviz(root: AVLTreeNode,
                data_maxlen: Optional[int] = None,
                show_subtree_props: bool = False) -> graphviz.Digraph:
    """Returns a `graphviz.Digraph` representing the tree rooted at `root`.

    The label of each node `x` in the `Digraph` includes:
    * The node's display name, `x.display_name`
    * If `data_maxlen` is set, a string representation of `x.data` truncated to
      `data_maxlen` characters
    * If `show_subtree_props` is set, a printout of `x.subtree_props`.

    The returned object can be rendered by calling `.render(...)` [1].

    The GraphViz object constructed in this function uses the trick described
    in the FAQ [2] for symmetrizing the tree layout so that it looks like a
    conventional drawing of a binary tree.  It also uses GraphViz "cluster"
    subgraphs to ensure that the subtrees of sibling nodes have disjoint
    bounding boxes.  The resulting graph layout is not perfect, but it's pretty
    good and it's roughly the best a GraphViz-supported layout engine can do
    without instance-specific manual tuning.

    [1] https://graphviz.readthedocs.io/en/stable/api.html#graphviz.Digraph.render
    [2] https://graphviz.org/faq/#FaqBalanceTree
    """
    def label_of(node: AVLTreeNode):
        return viz_node_label(node, data_maxlen=data_maxlen,
                              show_subtree_props=show_subtree_props)

    assert root is not None
    g = graphviz.Digraph()
    g.node(viz_node_id(root), label_of(root))

    def recurse(node, graph):
        node_id = viz_node_id(node)
        if children(node):
            # Dummy display text to make invisible nodes be approx the same
            # size as visible nodes
            placeholder_display_text = label_of(children(node)[0])

            child_ids = []

            def add_child(direction: str,
                          child: Optional[AVLTreeNode],
                          *,
                          phantom_text: str = placeholder_display_text):
                if child is None:
                    child_id = f"{node_id}_phantom_{direction}"
                    h.node(child_id, placeholder_display_text, style="invis")
                    h.edge(node_id, child_id, style="invis")
                else:
                    child_id = viz_node_id(child)
                    h.node(child_id, label_of(child))
                    h.edge(node_id, child_id)
                child_ids.append(child_id)

            with graph.subgraph(name=f"cluster_at_{node_id}",
                                graph_attr={"style": "invis"}) as h:
                add_child("left", node.left)
                add_child("middle", None, phantom_text="")
                add_child("right", node.right)
                for child in children(node):
                    recurse(child, h)

    recurse(root, g)
    return g
