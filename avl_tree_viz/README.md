# AVL Tree Viz Library

Extra viz functionality for `avl_tree.core`.

Currently, the only viz backend used by this library is GraphViz.  Compared to
the built-in pretty-printer `avl_tree.core.pprint`, the GraphViz rendering has:

* The ability to include more information in the node labels (`data` attribute,
  `subtree_props` dict)
* More graceful behavior when node display names are long
* Pretty good graph layout, but not perfect.  GraphViz's layout engine, while
  powerful, is not tailored to ordered trees or non-full binary trees.  We do
  however use the tricks suggested in the
  [FAQ](https://graphviz.org/faq/#FaqBalanceTree) to get the best layout we
  can.

## How to install

First install `avl_tree.core`.

Then install the package: from the directory containing this readme, run
```shell
python3 -m pip install .
```
